import { Component, Inject, ViewChild } from '@angular/core';
import { QUILL_CONFIG_TOKEN, QuillConfig, QuillEditorComponent } from 'ngx-quill'

import Quill from 'quill'
import Counter from './counter';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('editor') quillEditor: QuillEditorComponent | undefined
  logChange(event:any){
    console.log(event);
    
  }
  logSelection(event:any){
    console.log(event);
    
  }

  ajoutSignature(){
    if(this.quillEditor){
      console.log(this.quillEditor);
      this.text = this.quillEditor.content + this.dangerousModel
      // _text = new XMLSerializer().serializeToString(bodyTextHtml);
      let htmlElement = new DOMParser().parseFromString(this.text, "text/html")
      console.log(this.quillEditor.content);
      
    }
  }
  constructor(){
    
  }
  text = "<p>text</p>"
  dangerousModel = `<div class="signature" style="font-size: 14px;font-family: Segoe UI;">
  <p class="secureSpace" style="font-size:14px; font-family:Segoe UI">&nbsp;</p>
  <p><br><span style="font-family: &quot;Segoe UI&quot;;">Cordialement,</span><br>-- <br></p>
  <table style="border:none;">
      <tbody>
          <tr>
              <td><a href="http://www.arkane-foncier.fr">
                      <img src="http://arkane-foncier.fr/sig/logo_arkane-ge.png" style="display:inline;border:none;"
                          title="Arkane-foncier, géomètres-experts associés
numéro d'inscription à l'OGE 1998C200004
www.arkane-foncier.fr" alt="Logo Arkane foncier/GE" moz-do-not-send="false" 
                          class="e-rte-image e-imginline"></a></td>
              <td class="">
                  <center><span style="font-family: Tahoma, Geneva, sans-serif;">Olivier </span><span
                          style="font-variant:small-caps;"><span
                              style="font-family: Tahoma, Geneva, sans-serif;">Meyer</span></span><span
                          style="font-family: Tahoma, Geneva, sans-serif;">, Chef de mission</span><br><b>Arkane
                          foncier</b>, Géomètres-experts associés<br><a href="geo:48.637535,2.271627"
                          title="Naviguez vers le 17, grande rue à Montlhéry (si vous consultez ce courriel sur votre téléphone portable)">17,
                          grande rue, 91311 <span style="font-variant:small-caps;">Montlhéry</span> cedex</a>
                      <br>T : <a href="" title="Call +331 64 49 55 74 via 3CX" tcxhref="+33164495574"
                          target="_blank">01 64 49 55 74</a> F : 01 64 49 06 64<br><a
                          href="http://www.arkane-foncier.fr" alt="Visitez notre site web"
                          title="Suivez nous sur Internet"><img src="http://arkane-foncier.fr/sig/www.png"
                              moz-do-not-send="false"  class="e-rte-image e-imginline"></a>&nbsp;<a
                          href="https://www.linkedin.com/company/arkane-foncier" alt="Suivez nous sur LinkedIn"
                          title="Suivez nous sur LinkedIn"><img src="http://arkane-foncier.fr/sig/linkedin.png"
                              moz-do-not-send="false"  class="e-rte-image e-imginline"></a>&nbsp;<a
                          href="https://www.facebook.com/pg/arkanefoncier/posts/?ref=page_internal"
                          alt="Suivez nous sur facebook" title="Suivez nous sur facebook">

                          <img src="http://arkane-foncier.fr/sig/facebook.png" moz-do-not-send="false" 
                              class="e-rte-image e-imginline"></a>&nbsp;<a
                          href="https://www.youtube.com/channel/UCU-Ek3JefBsklwcBeclsV3w"
                          alt="Suivez nous sur youtube" title="Suivez nous sur youtube"><img
                              src="http://arkane-foncier.fr/sig/youtube.png" moz-do-not-send="false" 
                              class="e-rte-image e-imginline"></a>&nbsp;<a
                          href="https://arkane-foncier.wetransfer.com"
                          alt="Envoyez nous vos documents (jusqu'à 20 Go) sur wetransfer"
                          title="Envoyez nous vos documents (jusqu'à 20 Go) par wetransfer"><img
                              src="http://arkane-foncier.fr/sig/wetransfer.png" moz-do-not-send="false" 
                              class="e-rte-image e-imginline"></a>
                  </center>
              </td>
          </tr>
      </tbody>
  </table>
  <p><a href="http://arkane-foncier.fr/?page_id=10"><img src="http://arkane-foncier.fr/sig/actu.jpg"
              title="Dernières nouvelles d'Arkane foncier" class="e-rte-image e-imginline"></a><br><br></p>
  <p><b><span style="color:lime;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;">Avant d'imprimer, pensez à
              l'environnement</span></b></p>
  <p><span style="font-size:7.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:navy;">Ce message et
          toutes les pièces jointes sont établis à l'intention exclusive de ses destinataires et sont confidentiels.
          Si vous recevez ce message par erreur, merci d'en avertir immédiatement l'expéditeur et de le détruire.
          Toute utilisation de ce message non conforme à sa destination, toute diffusion ou toute publication, totale
          ou partielle, est interdite, sauf autorisation expresse. L'internet ne permettant pas d'assurer l'intégrité
          de ce message, la SELÀRL ARKANE FONCIER décline toute responsabilité au titre de celui-ci, dans l'hypothèse
          où il aurait été modifié.</span></p>
  <p><br></p>
</div>`


  title = 'geoprodquiller';

  editorModel : any = [{
    attributes: {
      font: 'roboto'
    },
    insert: 'test'
  }]

}
