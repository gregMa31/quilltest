import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { QuillModule } from 'ngx-quill'
import Quill from 'quill';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// const font = Quill.import('formats/font')
// const FontAttributor = Quill.import('attributors/class/font');
// FontAttributor.whitelist = [
//   'IRANSans',
//   'roboto',
//   'cursive',
//   'fantasy',
//   'monospace'
// ];
// Quill.register(FontAttributor, true);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    QuillModule.forRoot()
    // QuillModule.forRoot({
    //   modules: {
    //     toolbar: [
    //       ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    //       ['blockquote', 'code-block'],
      
    //       [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    //       [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    //       [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    //       [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    //       [{ 'direction': 'rtl' }],                         // text direction
      
    //       [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    //       [{ 'header': [1, 2, 3, 4, 5, 6,7,8, false] }],
      
    //       [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    //       [{ font: ['IRANSans', 'roboto', 'cursive', 'fantasy', 'monospace'] }],
    //       [{ 'align': [] }],
      
    //       ['clean'],                                         // remove formatting button
      
    //       ['link', 'image', 'video']                         // link and image, video
    //     ]
    //   }
    // })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
